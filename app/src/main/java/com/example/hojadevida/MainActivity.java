package com.example.hojadevida;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    //metodo para el boton siguiente
    public void Siguiente(View view){
        Intent siguien=new Intent(this, SegundoActivity.class);
        startActivity(siguien);
    }
    //metodo para el boton Ilfr
    public void mensajeIlfr(View view){
        Toast.makeText(this,"Ingrid", Toast.LENGTH_LONG).show();
    }
}